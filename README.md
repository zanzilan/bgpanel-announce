Bright Game Panel Announcer
---------------------------
Collection of scripts for automatically adding game server nodes to an existing BGPanel installation.
Uses PHP/libcurl to make JSon calls to a modified BGPanel installation. Especially useful for gameservers that are provisioned on-the-fly through Chef, Ansible, PXERoot, etc.

# Requirements
* PHP 5.2+
* libcurl
* systemd
* iproute2

# Functionality
The script uses cURL to POST a JSON-encoded array to with the following contents:
* apikey - Configurable at the top of the document, should be moved to an included configuration file asap
* hostname - The hostname of the machine, attained by executing the PHP gethostname() function
* user - The name of the SSH user to be connected to by BGPanel
* ssh_port - Attained by get_ssh_port(), a custom function that scrapes netstat
* ip_addr[] - An Array constructed by get_ipv4_ifaces()
  * id - The interface ID as stated by iproute2
  * name - The interface name (for reference/debugging)
  * addr - The actual address of the network interface
