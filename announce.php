<?php
/**
 * Bright Game Panel Gameserver Announcer
 * Only works with IPv4 for the time being
 */

include('config.php.inc');

# get_ipv4_ifaces()
# Get the server's local IPv4 addresses from iproute2
function get_ipv4_ifaces() {
  # Execute system command and split line by line
  $out = shell_exec("env ip ad");

  # DEBUG
  #var_dump($out);

  # Initialize
  $local_addrs = array();
  $ifname = 'unknown';
  $matches = array();

  if (preg_match_all("#(\d{1,2}):\s([a-z0-9]+[^lo]):\s.+\n.+link.+\n.+inet\s(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/\d{1,2}#", $out, $matches, PREG_SET_ORDER)) {
    # Build JSON document
    foreach($matches as &$m) {
      # Shift first element (whole regex match) off the beginning of the array
      array_shift($m);
      $m = array(
        "id" => $m[0],
        "name" => $m[1],
        "addr" => $m[2]);
    }
    # Return found interfaces
    return $matches;
  }
}

# get_sshd_port()
# Runs netstat to find out about the running SSH daemon
# Works fine on non-privileged users
function get_sshd_port() {
  $matches = array();
  $sshd = shell_exec("netstat -tuplen 2> /dev/null");
  if(preg_match("/^tcp\s.+:(\d{1,5}).+sshd/", $sshd, $matches)) {
    # Return the first match
    return $matches[1];
  } else {
    # Return default port
    return 22;
  }
}

# Construct the array sent in the the request
$req = array(
  "apikey"    => $apiKey,
  "hostname"  => gethostname(),
  "user"      => $user,
  "ssh_port"  => get_sshd_port(),
  "ip_addr"   => get_ipv4_ifaces()
  );

# Encode the document
$data_string = json_encode($req);

$ch = curl_init('http://' . $url . '/announce.php');
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
      );

# Make the request
$result = curl_exec($ch);

echo $result;
